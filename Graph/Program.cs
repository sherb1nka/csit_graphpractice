﻿using System;

namespace Graph // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GraphUI graphUI = GraphUI.Instance;
            graphUI.StartGraphUI();

            //GraphClass graph = new GraphClass("DNW.txt");
            //GraphClass graphCopy = (GraphClass)graph.Clone();
            //graph.DeleteVertex("1");
            //graph.ToFile("result1.txt");
            //graphCopy.ToFile("result2.txt");

        }
    }
}
