﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph
{
    public class GraphClass : ICloneable
    {
        private Dictionary<string, Dictionary<string, int>> _graph;
        private List<Tuple<string, string, int>> _edges;
        private List<string> _visitedNodes;
        private Queue<string> _edgesQueue;
        private Dictionary<string, int> _nodes;

        private int[,] _matrix;
        public int this[int i, int j] { get => _matrix[i, j]; set => _matrix[i, j] = value; }
        public int Size => _matrix.GetLength(0);

        public Dictionary<string, Dictionary<string, int>> Graph { get => _graph; }

        public List<Tuple<string, string, int>> Edges 
        {
            get
            {
                if (_edges != null)
                {
                    List<Tuple<string, string, int>> edges = new List<Tuple<string, string, int>>();
                    foreach (var node in Graph)
                    {
                        foreach (var vertice in node.Value)
                        {
                            try
                            {
                                edges.Add(new Tuple<string, string, int>(node.Key, vertice.Key, vertice.Value));
                            }
                            catch { }

                        }
                    }
                    return edges;
                }
                else return null;
            }
        }

        public List<string> VisitedNodes { get => _visitedNodes; }

        public bool IsWeighted { get; private set; } = true;
        public bool IsDirected { get; private set; } = true;

        public GraphClass()
        {
            _graph = new Dictionary<string,Dictionary<string,int>>();
            _edges = new List<Tuple<string, string, int>>();
            _visitedNodes = new List<string>();
            _nodes = new Dictionary<string, int>();
        }


        public GraphClass(string file)
        {
            _graph = new Dictionary<string, Dictionary<string, int>>();
            _edges = new List<Tuple<string, string, int>>();
            _visitedNodes = new List<string>();
            _nodes = new Dictionary<string, int>();

            using (StreamReader streamReader = new StreamReader(file))
            {
                string line;
                string[] vertWeightLine;
                IsDirected = bool.Parse(streamReader.ReadLine());
                IsWeighted = bool.Parse(streamReader.ReadLine());

                int j = 0;
                while ((line = streamReader.ReadLine()) != null)
                {
                    Dictionary<string, int> currentVertices = new Dictionary<string, int>();
                    string[] v = line.Split(' ');
                    for (int i = 1; i < v.Length; ++i)
                    {
                        if (IsWeighted)
                        {
                            vertWeightLine = v[i].Split(':');
                            currentVertices.Add(vertWeightLine[0], int.Parse(vertWeightLine[1]));
                        }
                        else currentVertices[v[i]] = 0;
                    }
                    Graph.Add(v[0], currentVertices);
                    _nodes.Add(v[0], j++);
                }
            }
        }

        //вместо конструктора-копии реализую интерфейс ICloneable
        //и делаю глубокое копирование
        public object Clone()
        {
            GraphClass graphCopy = new GraphClass();
            graphCopy.IsDirected = IsDirected;
            graphCopy.IsWeighted = IsWeighted;

            foreach(var vertice in _graph)
            {
                Dictionary<string, int> tempDic = new Dictionary<string, int>();
                foreach(var item in vertice.Value)
                {
                    tempDic.Add(item.Key, item.Value);
                }
                graphCopy.Graph[vertice.Key] = tempDic;
            }

            foreach(var pair in Edges)
            {
                graphCopy.Edges.Add(new Tuple<string, string, int>(pair.Item1, pair.Item2, pair.Item3));
            }

            foreach(var item in VisitedNodes)
            {
                graphCopy.VisitedNodes.Add(item);
            }

            return graphCopy;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertex"></param>
        /// <exception cref="ArgumentException">Такая вершина уже существует</exception>
        public void AddVertex(string vertex)
        {
            if (Graph.ContainsKey(vertex))
                throw new ArgumentException();
            Graph[vertex] = new Dictionary<string, int>();
            _nodes.Add(vertex, Graph.Count-1);
        }

        /// <param name="vertex1"></param>
        /// <param name="vertex2"></param>
        /// <exception cref="ArgumentNullException">Не существует одного/обоих рёбер</exception>
        /// <exception cref="ArgumentException">Такое ребро уже существует</exception>
        public void AddEdge(string vertex1, string vertex2)
        {
            if (!Graph.ContainsKey(vertex2))
                throw new KeyNotFoundException();
            Graph[vertex1].Add(vertex2, 0);
            if(!IsDirected)
                Graph[vertex2].Add(vertex1, 0);
        }

        /// <param name="vertex1"></param>
        /// <param name="vertex2"></param>
        /// <exception cref="ArgumentNullException">Не существует одного/обоих рёбер</exception>
        /// <exception cref="ArgumentException">Такое ребро уже существует</exception>
        public void AddEdge(string vertex1, string vertex2, int weight)
        {
            if (!Graph.ContainsKey(vertex2))
                throw new KeyNotFoundException();
            Graph[vertex1].Add(vertex2, weight);
            if(!IsDirected)
                Graph[vertex2].Add(vertex1, weight);
        }


        /// <summary>
        /// </summary>
        /// <param name="vertex"></param>
        /// <exception cref="ArgumentNullException">Нет такой вершины</exception>
        public void DeleteVertex(string vertex)
        {
            if (!Graph.Remove(vertex))
                throw new KeyNotFoundException();
            foreach(var item in Graph.Keys)
            {
                Graph[item].Remove(vertex);
                _nodes.Remove(item);
            }
        }

        /// <exception cref="ArgumentNullException">Нет такого ребра</exception>
        public void DeleteEdge(string vertex1, string vertex2)
        {
            if(!Graph[vertex1].Remove(vertex2))
                throw new KeyNotFoundException();
            if (!IsDirected)
                if (!Graph[vertex2].Remove(vertex1))
                    throw new KeyNotFoundException();
        }

        public void ToFile(string filePath)
        {
            using (StreamWriter streamWriter = new StreamWriter(filePath))
            {
                streamWriter.WriteLine(IsDirected);
                streamWriter.WriteLine(IsWeighted);
                foreach (var node in Graph)
                {
                    streamWriter.Write($"{node.Key} ");
                    foreach(var vertices in node.Value)
                    {
                        streamWriter.Write($"{vertices.Key}");
                        if (IsWeighted)
                            streamWriter.Write($":{vertices.Value}");
                        streamWriter.Write(" ");
                    }
                    streamWriter.WriteLine();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException">Граф не ориентированный</exception>
        public Dictionary<string, int> Degrees()
        {
            if (!IsDirected)
                throw new NotImplementedException();

            Dictionary<string, int> result = new Dictionary<string, int>();

            foreach(var node in Graph)
            {
                result[node.Key] = node.Value.Count;
                foreach(var vertice in Graph)
                {
                    foreach(var adjver in vertice.Value)
                    {
                        if (adjver.Key == node.Key)
                            result[node.Key]++;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException">Нет данной вершины</exception>
        public List<string> NotAdjacent(string vertex)
        {
            if (!Graph.ContainsKey(vertex))
                throw new KeyNotFoundException();
            List<string> notAdjVertices = new List<string>();
            foreach(var node in Graph)
            {
                bool isAdjacent = false;
                foreach (var vertice in node.Value)
                {
                    if (vertice.Key == vertex)
                    {
                        isAdjacent = true;
                        break;
                    }
                }

                if (!isAdjacent)
                    notAdjVertices.Add(node.Key);
            }

            return notAdjVertices;
        }

        public static GraphClass SymmDiff(GraphClass graph1, GraphClass graph2)
        {
            GraphClass result = (GraphClass)graph1.Clone();
            foreach(var node in graph2.Graph)
            {
                try
                {
                    result.AddVertex(node.Key);
                }
                catch (ArgumentException)
                { }
            }

            foreach(var pair in graph1.Edges)
            {
                if(!graph2.Edges.Contains(pair))
                {
                    try
                    {
                        result.AddEdge(pair.Item1, pair.Item2, pair.Item3);
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        result.DeleteEdge(pair.Item1, pair.Item2);
                    }
                    catch { }
                }
            }
            foreach(var pair in graph2.Edges)
            {
                if(!graph1.Edges.Contains(pair))
                {   try
                    {
                        result.AddEdge(pair.Item1, pair.Item2, pair.Item3);
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        result.DeleteEdge(pair.Item1, pair.Item2);
                    }
                    catch { }
                }
            }

            return result;
        }

        private bool Dfs(string currentVertex,string targetVertex)
        {
            if (currentVertex == targetVertex)
                return true;
            if (VisitedNodes.Contains(currentVertex))
                return false;

            VisitedNodes.Add(currentVertex);

            foreach(var vertice in Graph[currentVertex])
            {
                if(!VisitedNodes.Contains(vertice.Key))
                {
                    bool reached = Dfs(vertice.Key, targetVertex);
                    if (reached)
                        return true;
                }
            }

            return false;

        }

        public List<string> GetCycles(string vertex)
        {
            List<string> result = new List<string>();
            foreach(var node in Graph.Keys)
            {
                _visitedNodes.Clear();
                if (Dfs(vertex, node))
                {
                    _visitedNodes.Clear();
                    if (Dfs(node, vertex))
                    {
                        result.Add(node);
                    }
                }

            }
            

            return result;
        }

        public bool Bfs(string u, string v)
        {
            _edgesQueue = new Queue<string>();
            _edgesQueue.Enqueue(u);
            _visitedNodes.Add(u);
            while(_edgesQueue.Count > 0)    
            {
                foreach(var item in Graph[_edgesQueue.Dequeue()])
                {
                    if(!_visitedNodes.Contains(item.Key))
                    {
                        _edgesQueue.Enqueue(item.Key);
                        _visitedNodes.Add(item.Key);
                        if (item.Key == v)
                            return true;

                    }
                }
            }
            return false;
        }
        
        public void FindBfs(List<string> path, string u, string v)
        {
            if (u == v)
            {
                path.Add(u);
                foreach (string node in path)
                {
                    Console.Write($"{node} ");
                }
                Console.WriteLine();
            }
            else 
            { 
                _visitedNodes.Clear();
                path.Add(u);
                if (Bfs(u, v))
                {
                    foreach(var item in Graph[u])
                    {
                        FindBfs(path, item.Key, v);
                        path.RemoveAt(path.Count - 1);
                    }
                }
            }
        }

        public GraphClass MST()
        {
            List<Link> notUsedLinks = new List<Link>();
            List<string> notUsedNodes = new List<string>();
            List<string> usedNodes = new List<string>();
            List<Link> linksMST = new List<Link>();

            GraphClass result = new GraphClass();
            result.IsDirected = false;
            result.IsWeighted = true;
            foreach(var item in Graph)
            {
                result.AddVertex(item.Key);
            }
            
            foreach(var item in Graph.Keys)
            {
                notUsedNodes.Add(item);
                
                foreach(var node in Graph[item])
                {
                    notUsedLinks.Add(new Link(item, node.Key, node.Value));
                }
            }
            usedNodes.Add(notUsedNodes[0]);
            notUsedNodes.RemoveAt(0);

            while(notUsedNodes.Count > 0)
            {
                int minLinkIndex = -1;

                for(int i=0; i<notUsedLinks.Count; ++i)
                {
                    if (usedNodes.Contains(notUsedLinks[i].v1) && notUsedNodes.Contains(notUsedLinks[i].v2) ||
                        usedNodes.Contains(notUsedLinks[i].v2) && notUsedNodes.Contains(notUsedLinks[i].v1))
                    {
                        if(minLinkIndex != -1)
                        {
                            if (notUsedLinks[i].weight < notUsedLinks[minLinkIndex].weight)
                                minLinkIndex = i;
                        }
                        else
                            minLinkIndex = i;
                    }
                }

                if(usedNodes.Contains(notUsedLinks[minLinkIndex].v1))
                {
                    usedNodes.Add(notUsedLinks[minLinkIndex].v2);
                    notUsedNodes.Remove(notUsedLinks[minLinkIndex].v2);
                }
                else
                {
                    usedNodes.Add(notUsedLinks[minLinkIndex].v1);
                    notUsedNodes.Remove(notUsedLinks[minLinkIndex].v1);
                }

                linksMST.Add(notUsedLinks[minLinkIndex]);
                notUsedLinks.RemoveAt(minLinkIndex);
            }

            foreach(var item in linksMST)
            {
                result.AddEdge(item.v1, item.v2, item.weight);
            }

            return result;
        }

        public string[,] Floyd()
        {
            int[,] result = new int[Graph.Count, Graph.Count];
            string[,] indexes = new string[Graph.Count, Graph.Count];

            string t = "";
            foreach (var u in Graph.Keys)
            {
                foreach (var j in Graph.Keys)
                {
                    if (u == j)
                    {
                        result[_nodes[u], _nodes[j]] = 0;
                        indexes[_nodes[u], _nodes[j]] = j;
                    }
                    else
                    {
                        if (!Graph[u].ContainsKey(j))
                        {
                            result[_nodes[u], _nodes[j]] = 99_999_999;
                        }
                        else
                        {
                            if (IsWeighted)
                            {
                                result[_nodes[u], _nodes[j]] = Graph[u][j];
                            }
                            else
                            {
                                result[_nodes[u], _nodes[j]] = 1;
                            }
                        }
                    }
                }
            }

            for (int k = 0; k < Graph.Count; ++k)
            {
                for (int i = 0; i < Graph.Count; ++i)
                {
                    for (int j = 0; j < Graph.Count; ++j)
                    {
                        if (result[i, j] > result[i, k] + result[k, j] && i != j)
                        {
                            result[i, j] = result[i, k] + result[k, j];
                            indexes[i, j] = Graph.ElementAt(k).Key;
                        }
                    }
                }
            }
            return indexes;
        }

        public void FloydPath(string u, string v, string[,] indexes)
        {
            if (indexes[_nodes[u], _nodes[v]] != null)
            {
                FloydPath(u, indexes[_nodes[u], _nodes[v]],  indexes);
                Console.Write($"{indexes[_nodes[u], _nodes[v]]} "); 
                FloydPath(indexes[_nodes[u], _nodes[v]], v, indexes);
            }
        }

        public int Dijkstra(string u, string v)
        {
            Dictionary<string, int> s = new Dictionary<string, int>();
            s.Add(u, 0);
            List<string> notVisitedNodes = Graph.Keys.ToList();

            while(true)
            {
                string toOpen = null;
                int weight = int.MaxValue;
                foreach(var node in notVisitedNodes)
                {
                    if(s.ContainsKey(node) && s[node]<weight)
                    {
                        toOpen = node;
                        weight = s[node];
                    }
                }
                if (toOpen == null)
                    throw new NullReferenceException("Пути нет");
                if (toOpen == v)
                    break;
                foreach(var node in Graph[toOpen])
                {
                    int currentWeight = s[toOpen] + node.Value;
                    if (!s.ContainsKey(node.Key) || s[node.Key] > currentWeight)
                        s[node.Key] = currentWeight;
                }
                notVisitedNodes.Remove(toOpen);
            }

            return s[v];
        }

        public Tuple<bool, List<string>> BelFord(string v, string t)
        {
            int[] dist = new int[Graph.Count];
            List<int> p = new List<int>();
            bool hasNegativeCircle = false;

            foreach (var u in Graph.Keys)
            {
                dist[_nodes[u]] = int.MaxValue;
                p.Add(-1);
            }
            dist[_nodes[v]] = 0;

            List<Link> ed = new List<Link>();
            List<string> verts = new List<string>();

            foreach (var item in Graph.Keys)
            {
                verts.Add(item);
                foreach (var elem in Graph[item])
                {
                    Link temp = new Link(item, elem.Key, elem.Value);
                    ed.Add(temp);
                }
            }

            int x = -1;
            for (int i = 0; i < Graph.Keys.Count; i++)
            {
                x = -1;
                for (int j = 0; j < ed.Count; j++)
                {
                    string v1 = ed[j].v1;
                    string v2 = ed[j].v2;
                    int weight = ed[j].weight;
                    if (dist[_nodes[v1]] != int.MaxValue && dist[_nodes[v1]] + weight < dist[_nodes[v2]])
                    {
                        dist[_nodes[v2]] = dist[_nodes[v1]] + weight;
                        p[_nodes[v2]] = _nodes[v1];
                        x = _nodes[v2];
                    }
                }
            }

            for (int i = 0; i < ed.Count; i++)
            {
                string v1 = ed[i].v1;
                string v2 = ed[i].v2;
                int weight = ed[i].weight;

                if (dist[_nodes[v1]] != int.MaxValue && dist[_nodes[v1]] + weight < dist[_nodes[v2]])
                    hasNegativeCircle = true;
            }

            List<string> path = new List<string>();
            if (x == -1)
            {
                for (int cur = _nodes[t]; cur != -1; cur = p[cur])
                {
                    path.Add(_nodes.ElementAt(cur).Key);
                }
                path.Reverse();
            }
            else
            {
                int y = x;
                for (int i = 0; i < Graph.Keys.Count; i++)
                {
                    y = p[y];
                }
                for (int cur = y; ; cur = p[cur])
                {
                    path.Add(_nodes.ElementAt(cur).Key);
                    if (cur == y && path.Count > 1)
                    {
                        break;
                    }
                }
                path.Reverse();
            }
            return new Tuple<bool, List<string>>(hasNegativeCircle, path);
        }

        public int MaxFlow(string s, string t)
        {
            int[,] capacity = new int[Graph.Count, Graph.Count];
            foreach (var u in Graph.Keys)
            {
                foreach (var j in Graph.Keys)
                {
                    if (u == j)
                    {
                        capacity[_nodes[u], _nodes[j]] = 0;
                    }
                    else
                    {
                        if (!Graph[u].ContainsKey(j))
                        {
                            capacity[_nodes[u], _nodes[j]] = 0;
                        }
                        else
                        {
                            capacity[_nodes[u], _nodes[j]] = Graph[u][j];
                        }
                    }
                }
            }
            for (int flow=0; ;)
            {
                int df = FindPath(capacity, new bool[capacity.GetLength(1)], s, t, int.MaxValue);
                if (df == 0)
                    return flow;
                flow += df;
            }
        }

        private int FindPath(int[,] cap, bool[] visited, string u, string t, int f)
        {
            if (u == t)
                return f;
            visited[_nodes[u]] = true;
            for(int v = 0; v < visited.Length; ++v)
            {
                if(!visited[v] && cap[_nodes[u], v] > 0)
                {
                    int df = FindPath(cap, visited, _nodes.ElementAt(v).Key, t, Math.Min(f, cap[_nodes[u], v]));
                    if(df > 0)
                    {
                        cap[_nodes[u], v] -= df;
                        cap[v, _nodes[u]] += df;
                        return df;
                    }
                }
            }
            return 0;
        }
    }
}
