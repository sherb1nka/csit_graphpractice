﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph
{
    public class GraphUI
    {
        private static GraphUI _instance;
        private GraphClass graph;
        public static GraphUI Instance => _instance ?? (_instance = new GraphUI());
        public static void CleareInstance()
        {
            _instance = null;
        }

        public void StartGraphUI()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Введите путь к графу: ");
                try
                {
                    graph = new GraphClass(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message}");
                    Console.ReadLine();
                }
            }

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Доступные опции:\n" +
                                  "1. Вывести список смежности в консоль\n" +
                                  "2. Вывести список смежности в файл\n" +
                                  "3. Добавить вершину\n" +
                                  "4. Удалить вершину\n" +
                                  "5. Добавить ребро(дугу)\n" +
                                  "6. Удалить ребро(дугу)\n" +
                                  "7. Вывести степени вершин орграфа\n" +
                                  "8. Вывести вершины несмежные с указанной\n" +
                                  "9. Создать являющийся симметрической разностью по дугам двух заданных орграфов граф\n" +
                                  "10. Найти все достижимые из данной вершины орграфа, из которых можно попасть обратно в данную вершину.\n" +
                                  "11. Вывести все пути из u в v\n" +
                                  "12. Вывести каркас(минимальное остовное дерево) графа\n" +
                                  "13. Вывести длины кратчайших путей от u до v1 и v2.\n" +
                                  "14. Вывести кратчайшие пути для всех пар вершин.\n" +
                                  "15. Вывести кратчайший путь из вершины u до вершины v.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.Clear();
                        ShowGraph();
                        Console.ReadLine();
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("Введите путь к файлу");
                        graph.ToFile(Console.ReadLine());
                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine("Введите вершину");
                        try
                        {
                            graph.AddVertex(Console.ReadLine());    
                        }
                        catch(ArgumentException)
                        {
                            Console.WriteLine("Такая вершина уже существует");
                            Console.ReadLine();
                        }
                        break;
                    case "4":
                        Console.Clear();
                        Console.WriteLine("Введите вершину");
                        try
                        {
                            graph.DeleteVertex(Console.ReadLine());
                        }
                        catch (KeyNotFoundException)
                        {
                            Console.WriteLine("Нет такой вершины");
                            Console.ReadLine();
                        }
                        break;
                    case "5":
                        Console.Clear();
                        Console.WriteLine("Введите 1-ю и 2-ю вершины");                        
                        if(graph.IsWeighted)
                        {
                            Console.WriteLine("Введите вес");
                            try
                            {
                                graph.AddEdge(Console.ReadLine(), Console.ReadLine(), int.Parse(Console.ReadLine()));
                            }
                            catch (KeyNotFoundException)
                            {
                                Console.WriteLine("Не существует одного/обоих рёбер");
                                Console.ReadLine();
                            }
                            catch (ArgumentException)
                            {
                                Console.WriteLine("Такое ребро уже существует");
                                Console.ReadLine();
                            }
                        }
                        else
                        {
                            try
                            {
                                graph.AddEdge(Console.ReadLine(), Console.ReadLine());
                            }
                            catch (KeyNotFoundException)
                            {
                                Console.WriteLine("Не существует одного/обоих рёбер");
                                Console.ReadLine();
                            }
                            catch (ArgumentException)
                            {
                                Console.WriteLine("Такое ребро уже существует");
                                Console.ReadLine();
                            }
                        }
                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine("Введите 1-ю и 2-ю вершины");
                        try
                        {
                            graph.DeleteEdge(Console.ReadLine(), Console.ReadLine());
                        }
                        catch (KeyNotFoundException)
                        {
                            Console.WriteLine("Не существует одного из двух рёбер");
                            Console.ReadLine();
                        }
                        break;
                    case "7":
                        Console.Clear();
                        ShowDegrees();
                        Console.ReadLine();
                        break;
                    case "8":
                        Console.Clear();
                        Console.WriteLine("Введите вершину");
                        ShowNotAdjVertices(Console.ReadLine());
                        Console.ReadLine();
                        break;
                    case "9":
                        Console.Clear();
                        Console.WriteLine("Введите путь к графу1: ");
                        GraphClass graph1 = new GraphClass();
                        try
                        {
                            graph1 = new GraphClass(Console.ReadLine());
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"{ex.Message}");
                            Console.ReadLine();
                        }

                        Console.WriteLine("Введите путь к графу2: ");
                        GraphClass graph2 = new GraphClass();
                        try
                        {
                            graph2 = new GraphClass(Console.ReadLine());
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"{ex.Message}");
                            Console.ReadLine();
                        }
                        graph = GraphClass.SymmDiff(graph1, graph2);
                        Console.ReadLine();
                        break;
                    case "10":
                        Console.Clear();
                        Console.WriteLine("Введите вершину");
                        ShowCycleVertices(Console.ReadLine());
                        Console.ReadLine();
                        break;
                    case "11":
                        Console.Clear();
                        Console.WriteLine("Введите вершину 1-ю и 2-ю вершины");
                        graph.FindBfs(new List<string>(), Console.ReadLine(), Console.ReadLine());
                        Console.ReadLine();
                        break;
                    case "12":
                        Console.Clear();
                        GraphClass temp = (GraphClass)graph.Clone();
                        graph = graph.MST();
                        ShowGraph();
                        graph = temp;
                        Console.ReadLine();
                        break;
                    case "13":
                        Console.Clear();
                        Console.WriteLine("Введите вершины u, v1 и v2");
                        string u = Console.ReadLine();
                        string v1 = Console.ReadLine();
                        string v2 = Console.ReadLine();
                        Console.WriteLine($"Длина от {u}");
                        try
                        {
                            Console.Write($"до {v1}: {graph.Dijkstra(u, v1)}");
                        }
                        catch (NullReferenceException e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        try
                        {
                            Console.WriteLine($"до {v2}: {graph.Dijkstra(u, v2)}");
                        }
                        catch (NullReferenceException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        Console.ReadLine();
                        break;
                    case "14":
                        Console.Clear();
                        ShowPaths();
                        Console.ReadLine();
                        break;
                    case "15":
                        Console.Clear();
                        Console.WriteLine("Введите вершины u и v");
                        ShowBellFordResult(Console.ReadLine(), Console.ReadLine());
                        Console.ReadLine();
                        break;
                    case "16":
                        Console.Clear();
                        Console.WriteLine("Укажите вершину источника и стока");
                        Console.WriteLine($"{graph.MaxFlow(Console.ReadLine(), Console.ReadLine())}");
                        Console.ReadLine();
                        break;
                    default:
                        break;
                }

            }
        }

        private void ShowGraph()
        {
            foreach (var node in graph.Graph)
            {
                Console.Write($"{node.Key} => ");
                foreach (var vertices in node.Value)
                {
                    Console.Write($"{vertices.Key}");
                    if (graph.IsWeighted)   
                        Console.Write($":{vertices.Value}");
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }

        private void ShowDegrees()
        {
            try
            {
                Dictionary<string, int> graphDegrees = graph.Degrees();
                foreach(var node in graphDegrees)
                {
                    Console.WriteLine($"{node.Key}: {node.Value}");
                }
            }
            catch (NotImplementedException)
            {
                Console.WriteLine("Граф не является ориентированным");
            }
        }

        private void ShowNotAdjVertices(string vertex)
        {
            try
            {
                foreach(string node in graph.NotAdjacent(vertex))
                {
                    Console.WriteLine($"{node} ");
                }
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Данной вершины нет в графе");
            }
        }

        private void ShowCycleVertices(string vertex)
        {
            foreach(var item in graph.GetCycles(vertex))
            {
                Console.Write($"{item} ");
            }
        }

        private void ShowPaths()
        {
            string[,] nodes = graph.Floyd();
            Console.WriteLine("Пути:");
            for(int i=0; i<graph.Graph.Count; ++i)
            {
                Console.WriteLine($"От вершины {graph.Graph.ElementAt(i).Key}");
                for(int j=0; j<graph.Graph.Count; ++j)
                {
                    if(i!=j)
                    {
                        Console.Write($"до {graph.Graph.ElementAt(j).Key}: {graph.Graph.ElementAt(i).Key} ");
                        graph.FloydPath(graph.Graph.ElementAt(i).Key, graph.Graph.ElementAt(j).Key, nodes);
                        Console.WriteLine($"{graph.Graph.ElementAt(j).Key}");
                    }
                }
            }
        }

        private void ShowBellFordResult(string u, string v)
        {
            if (graph.BelFord(u, v).Item1)
            {
                Console.WriteLine("В графе присутствует отрицательный цикл: ");
                foreach (var node in graph.BelFord(u, v).Item2)
                {
                    Console.Write($"{node} ");
                }
            }
            else
            {
                Console.WriteLine($"Кратчайший путь от {u} до {v}:");
                foreach(var node in graph.BelFord(u,v).Item2)
                {
                    Console.Write($"{node} ");
                }
            }
        }


    }
}
